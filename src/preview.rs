extern crate image;
extern crate minifb;

use image::{Pixel, RgbImage};
use minifb::{Key, Scale, Window, WindowOptions};
use std::{thread, time};

pub fn display_image_window(image: &RgbImage) {
    let buffer = rgb_to_32(&image);

    //
    let mut window = Window::new(
        "Image Preview",
        image.width() as usize,
        image.height() as usize,
        WindowOptions {
            borderless: false,
            resize: false,
            title: true,
            scale: Scale::X1,
        },
    )
    .unwrap_or_else(|e| {
        panic!("{}", e);
    });

    //
    const MAX_FPS: u32 = 200;
    let min_frame_time = time::Duration::from_micros(1_000_000 / u64::from(MAX_FPS));

    while window.is_open() && !window.is_key_down(Key::Escape) {
        let frame_start = time::Instant::now();

        // Render
        {
            // We unwrap here as we want this code to exit if it fails.
            window.update_with_buffer(&buffer).unwrap();
        }

        // Wait if frame time remaining.
        let render_time = frame_start.elapsed();
        if render_time < min_frame_time {
            thread::sleep(min_frame_time - render_time);
        }
    }
}

fn rgb_to_32(image: &RgbImage) -> Vec<u32> {
    let total_pixels = image.width() as usize * image.height() as usize;
    let mut buffer: Vec<u32> = vec![0; total_pixels];

    for (i, pixel) in image.pixels().enumerate() {
        let rgb = pixel.to_rgb().data;

        buffer[i] = {
            let (r, g, b) = (u32::from(rgb[0]), u32::from(rgb[1]), u32::from(rgb[2]));

            (r << 16) + (g << 8) + (b)
        };
    }

    buffer
}
