extern crate nalgebra;

use nalgebra::Vector3;

pub struct Ray {
    pub origin: Vector3<f64>,
    pub direction: Vector3<f64>,
}

impl Ray {
    pub fn point_at_parameter(&self, t: f64) -> Vector3<f64> {
        self.origin + t * self.direction
    }
}
