mod preview;
mod ray;

extern crate image;
extern crate nalgebra;

use image::{Rgb, RgbImage};
use nalgebra::Vector3;

use ray::Ray;

fn hit_sphere(center: Vector3<f64>, radius: f64, ray: &Ray) -> f64 {
    let oc = ray.origin - center;

    let a = ray.direction.dot(&ray.direction);
    let b = 2.0 * oc.dot(&ray.direction);
    let c = oc.dot(&oc) - radius * radius;

    let discriminant = b * b - 4.0 * a * c;

    if discriminant < 0.0 {
        -1.0
    } else {
        (-b - discriminant.sqrt()) / (2.0 * a)
    }
}

fn color(ray: &Ray) -> Vector3<f64> {
    // Cast ray.
    let t = hit_sphere(Vector3::new(0.0, 0.0, -1.0), 0.5, &ray);

    // Check if ray intersects with sphere.
    if t > 0.0 {
        let normal = (ray.point_at_parameter(t) - Vector3::new(0.0, 0.0, -1.0)).normalize();

        return 0.5 * (normal + Vector3::new(1.0, 1.0, 1.0));
    }

    // Lerp background color.
    let unit_direction = ray.direction.normalize();
    let bgt = 0.5 * (unit_direction.y + 1.0);

    let color1 = Vector3::new(1.0, 1.0, 1.0);
    let color2 = Vector3::new(0.0, 0.0, 0.0);

    (1.0 - bgt) * color1 + bgt * color2
}

fn to_rgb(col: &Vector3<f64>) -> Rgb<u8> {
    Rgb {
        data: [
            (col.x * 255.99) as u8,
            (col.y * 255.99) as u8,
            (col.z * 255.99) as u8,
        ],
    }
}

fn main() {
    let width = 800;
    let height = 600;

    let mut image = RgbImage::new(width, height);

    let horizontal = Vector3::new(8.0, 0.0, 0.0);
    let vertical = Vector3::new(0.0, 6.0, 0.0);

    // Center.
    let origin = Vector3::new(0.0, 0.0, 0.0);

    // Far plane.
    let far = Vector3::new(0.0, 0.0, -1.0);

    // Bottom left corner.
    let lower_left_corner = ((horizontal + vertical) / -2.0) + far;

    // Cast rays per pixel.
    for y in 0..height {
        for x in 0..width {
            // Joris?
            let percent_x = x as f64 / width as f64;
            let percent_y = y as f64 / height as f64;

            let ray = Ray {
                origin,
                direction: (lower_left_corner + percent_x * horizontal + percent_y * vertical),
            };

            //
            let color = color(&ray);
            image.put_pixel(x, y, to_rgb(&color));
        }
    }

    preview::display_image_window(&image);
}
